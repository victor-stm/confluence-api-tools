<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use VictorStm\confluence\content\Content;
use VictorStm\confluence\content\Document;
use VictorStm\confluence\content\Text;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsNumeric;


class DocumentTest extends TestCase
{
    public function testDocStructure()
    {
        $version        = 123;
        $title          = 'hello, world!';
        $id             = 9512347;

        $doc            = new Document ();
        $doc->version   = $version;
        $doc->title     = $title;
        $doc->id        = $id;

        $res = $doc->build ();

        assertEquals ($version, $res['version']['number'], 'version');
        assertEquals ($title, $res['title'], 'title');
        assertEquals ($id, $res['id'], 'id');
        assertEquals ($id, $res['body']['editor']['content']['id'], 'content.id');
        assertEquals ('{"type":"doc","content":[]}', $res['body']['editor']['value'], 'body.editor.value');
    }


    public function testDocsNodes()
    {
        $doc = new Document ();
        $doc->add (new Text('text 1'));
        $doc->add (new Text('text 2'));
        $doc->add (new Text('text 3'));

        $res        = $doc->build ();
        $content    = json_decode ($res['body']['editor']['value']);

        assertEquals ('text 1', $content->content[0]->text, 'text 1');
        assertEquals ('text 2', $content->content[1]->text, 'text 2');
        assertEquals ('text 3', $content->content[2]->text, 'text 3');
    }


    public function testDocsContentMultipleNodes()
    {
        $doc    = new Document ();
        $cont   = new Content ();
        $cont->add (new Text('text 1'));
        $cont->add (new Text('text 2'));
        $cont->add (new Text('text 3'));
        $doc->add ($cont);

        $res        = $doc->build ();
        $content    = json_decode ($res['body']['editor']['value']);

        assertEquals ('text 1', $content->content[0]->text, 'text 1');
        assertEquals ('text 2', $content->content[1]->text, 'text 2');
        assertEquals ('text 3', $content->content[2]->text, 'text 3');
    }


    public function testDocsContentSingleNode()
    {
        $doc    = new Document ();
        $cont   = new Content ();
        $cont->add (new Text('text 1'));
        $doc->add ($cont);

        $res        = $doc->build ();
        $content    = json_decode ($res['body']['editor']['value']);

        assertEquals ('text 1', $content->content[0]->text, 'text 1');
    }




}
