<?php

include "vendor\autoload.php";

use VictorStm\confluence\Confluence;
use VictorStm\confluence\content\Content;
use VictorStm\confluence\content\Header;
use VictorStm\confluence\content\Paragraph;
use VictorStm\confluence\content\Table;
use VictorStm\confluence\content\TableAttributes;
use VictorStm\confluence\content\TableCell;
use VictorStm\confluence\content\TableHeader;
use VictorStm\confluence\content\TableRow;
use VictorStm\confluence\content\Text;
use VictorStm\confluence\content\TextWithLink;


// config
$apiURL     = 'domain.without.schema';  // Correct: 'x.atlassian.net', WRONG: 'https://x.atlassian.net'
$apiToken   = 'user:api_token';
$pageID     = '111222';
$pageTitle  = 'Example test'; // set to '' for keep original page's title
$pageSpace  = 'IT';

$confAPI    = new Confluence ($apiURL, $apiToken);


//---------------------------------------------------------------------
// Content build
//---------------------------------------------------------------------

$content = new Content ();


//---------------------------------------------------------------------
// Headers
//---------------------------------------------------------------------

$h1 = new Header ('Header H1');
$h1->level = 1;

$h2 = new Header ('Header H2', 2);

$content->add ($h1)
    ->add ($h2)
    ->add (new Header ('Header H3, etc.', 3));


//---------------------------------------------------------------------
// Paragraph
//---------------------------------------------------------------------

$p = new Paragraph ('Hello, ');
$p->add (new Text('world!'))
    ->add (new Text(' And here is a link example: '))
    ->add (new TextWithLink('Google.com', 'https://google.com/'));

$content->add ($p);


//---------------------------------------------------------------------
// Table Node
//---------------------------------------------------------------------

$table = new Table ();
$table->attributes->layout = TableAttributes::LAYOUT_FULL_WIDTH;


//---------------------------------------------------------------------
// First row with header nodes
//---------------------------------------------------------------------

$headerRow = new TableRow ();
$table->add ($headerRow);

$headTime = new TableHeader ('Time');
$headTime->attributes->colwidth = [150];

$headProject = new TableHeader ('Project');
$headProject->attributes->colwidth = [220];

$headerRow->add ($headTime)
    ->add ($headProject)
    ->add (new TableHeader ('Message'));


//---------------------------------------------------------------------
// Rows
//---------------------------------------------------------------------

$table->add ((new TableRow())
                 ->add (new TableCell ('00:00:00'))
                 ->add (new TableCell ('Row 1 Project'))
                 ->add (new TableCell ('Row 1 Message'))
);

// OR

$row = new TableRow();
$row->add (new TableCell ('00:00:00'));
$row->add (new TableCell ('Row 2 Project'));
$row->add (new TableCell ('Row 2 Message'));
$table->add ($row);


//---------------------------------------------------------------------
// Add table to content & update existing page
//---------------------------------------------------------------------

$content->add ($table);

$confAPI->updatePage ($pageID, $content, $pageTitle, $pageSpace);
