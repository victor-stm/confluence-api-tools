<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/text/

namespace VictorStm\confluence\content;


class Text extends Element
{
    protected $type = 'text';

    public $text = '';

    /** @var TextMarks */
    public $marks;


    public function __construct ($text)
    {
        $this->text = $text;
        $this->marks = new TextMarks ();
    }


    public function build ()
    {
        $res            = parent::build ();
        $res['text']    = $this->text;
        $res['marks']   = $this->marks->build ();

        return $res;
    }

}
