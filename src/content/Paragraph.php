<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/paragraph/

namespace VictorStm\confluence\content;


class Paragraph extends AContent
{
    protected $type = 'paragraph';


    public function __construct ($content = '')
    {
        $this->importContent ($content);
    }


    public function build ()
    {
        return parent::build ();
    }

}
