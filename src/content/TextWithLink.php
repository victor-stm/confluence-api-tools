<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/text/

namespace VictorStm\confluence\content;


class TextWithLink extends Text
{
    public $link = '';


     public function __construct ($text, $link)
    {
        parent::__construct ($text);
        $this->link = $link;
    }


    public function build ()
    {
        $this->marks->type                  = TextMarks::MARK_LINK;
        $this->marks->attributes['href']    = $this->link;
        $this->marks->attributes['title']   = $this->text;

        $res = parent::build ();
        return $res;
    }

}
