<?php

namespace VictorStm\confluence\content;


class TableAttributes
{

    const LAYOUT_DEFAULT    = 'default';
    const LAYOUT_FULL_WIDTH = 'full-width';
    const LAYOUT_WIDE       = 'wide';

    /** @var bool */
    public $isNumberColumnEnabled = false;
    public $layout = self::LAYOUT_DEFAULT;


    public function build ()
    {
        return [
            "isNumberColumnEnabled" => $this->isNumberColumnEnabled,
            "layout"                => $this->layout
        ];
    }

}
