<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/table/

namespace VictorStm\confluence\content;


class Table extends Paragraph
{
    protected $type = 'table';

    /** @var TableAttributes */
    public $attributes;

    protected $allowedTypes = [
        TableRow::class
    ];


    public function __construct ($content = '')
    {
        parent::__construct ($content);
        $this->attributes = new TableAttributes;
    }


    public function build ()
    {
        $res            = parent::build ();
        $res['attrs']   = $this->attributes->build ();

        return $res;
    }

}
