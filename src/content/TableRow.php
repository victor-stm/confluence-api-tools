<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/table_row/

namespace VictorStm\confluence\content;


class TableRow extends Paragraph
{
    protected $type = 'tableRow';

    protected $allowedTypes = [
        TableCell::class,
        TableHeader::class
    ];

}
