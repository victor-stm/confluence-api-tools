<?php

namespace VictorStm\confluence\content;

class Content extends AContent
{

    public function build ()
    {
        $res = parent::build ();
        return $res['content'];
    }

}
