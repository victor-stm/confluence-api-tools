<?php

namespace VictorStm\confluence\content;


interface IElement
{
    public function build ();
}
