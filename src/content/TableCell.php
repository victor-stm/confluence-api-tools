<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/table_cell/

namespace VictorStm\confluence\content;


class TableCell extends Paragraph
{
    protected $type = 'tableCell';

    /** @var CellAttributes */
    public $attributes;

    protected $allowedTypes = [
        Header::class,
        Paragraph::class
    ];


    public function __construct ($content = '')
    {
        if (is_string ($content) || is_numeric ($content)) $content = new Paragraph ($content);
        parent::__construct ($content);
        $this->attributes = new CellAttributes ();
    }


    public function build ()
    {
        $res            = parent::build ();
        $res['attrs']   = $this->attributes->build ();

        return $res;
    }

}
