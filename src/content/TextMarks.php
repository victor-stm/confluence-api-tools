<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/text/

namespace VictorStm\confluence\content;


class TextMarks
{

    const MARK_NORMAL       = '';
    const MARK_CODE         = 'code';
    const MARK_EM           = 'em';
    const MARK_LINK         = 'link';
    const MARK_STRIKE       = 'strike';
    const MARK_STRONG       = 'strong';
    const MARK_SUBSUP       = 'subsup';
    const MARK_TEXTCOLOR    = 'textColor';
    const MARK_UNDERLINE    = 'underline';

    public $type = '';
    public $attributes = [];


    public function build ()
    {
        $res = [];

        if ($this->type != '') $res[] = ['type' => $this->type];
        if ($res && $this->attributes) $res[0]['attrs'] = $this->attributes;

        return $res;
    }

}
