<?php

namespace VictorStm\confluence\content;

class CellAttributes
{
    public $colspan = 1;
    public $rowspan = 1;
    public $colwidth = [];
    public $backgound = null;

    public function build ()
    {
        return [
            "colspan" => $this->colspan,
            "rowspan" => $this->rowspan,
            "colwidth" => $this->colwidth,
            "background" => $this->backgound
        ];
    }

}
