<?php

namespace VictorStm\confluence\content;

class Page extends Document
{
    protected $type = 'page';


    public function __construct ($content = '')
    {
        parent::__construct ($content);
    }


    public function build ()
    {
        return parent::build ();
    }

}
