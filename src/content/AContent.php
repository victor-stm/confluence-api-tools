<?php

namespace VictorStm\confluence\content;


abstract class AContent extends Element
{
    protected $content      = [];
    protected $allowedTypes = [];


    public function add (IElement $element)
    {
        if (count ($this->allowedTypes)) {
            $index = array_search (get_class ($element), $this->allowedTypes);
            if ($index === false) throw new \Exception ('Element of ' . get_class ($element) . ' not allowed');
        }

        $this->content[] = $element;
        return $this;
    }


    public function importContent ($content)
    {
        if (is_array ($content)) {
            foreach ($content as $element) $this->add ($element);
        } elseif ($content instanceof IElement) {
            $this->add ($content);
        } elseif (is_string ($content) && $content) {
            $this->add (new Text ($content));
        } elseif (is_numeric ($content) && $content) {
            $this->add (new Text ($content));
        }
    }


    public function build ()
    {
        $res            = parent::build ();
        $res['content'] = [];

        foreach ($this->content as $element) {
            $res['content'][] = $element->build();
        }

        return $res;
    }

}
