<?php

namespace VictorStm\confluence\content;

class Document extends AContent
{
    protected $type     = 'unknown';
    public $id          = '';
    public $title       = '';
    public $space       = '';
    public $version     = 0;


    public function __construct ($content = '')
    {
        $this->importContent ($content);
    }


    public function build ()
    {
        $content = [];
        foreach ($this->content as $element) {
            $content[] = $element->build();
        }

        if (count ($content) === 1) $content = $content[0];

        $res = [
            'id'        => $this->id,
            'status'    => 'current',
            'version'   => ['number' => $this->version],
            'type'      => $this->type,
            'title'     => $this->title,
            'space'     => ['key' => $this->space],
            'body'      => ['editor' => ['value' => [
                                            'type'      => 'doc',
                                            'content'   => $content
                                        ],
                                            'representation'    => 'atlas_doc_format',
                                            'content'           => ['id' => $this->id]
                                        ]
            ]
        ];

        // It should be string
        $res['body']['editor']['value'] = json_encode ($res['body']['editor']['value'], JSON_UNESCAPED_UNICODE);

        return $res;
    }

}
