<?php

// Doc: https://developer.atlassian.com/cloud/jira/platform/apis/document/nodes/heading/

namespace VictorStm\confluence\content;


class Header extends Paragraph
{
    protected $type = 'heading';

    public $level;


    public function __construct ($content = '', $level=1)
    {
        parent::__construct($content);
        $this->level    = $level;
    }


    public function build ()
    {
        $res            = parent::build ();
        $res['attrs']   = ['level'=>$this->level];

        return $res;
    }

}
