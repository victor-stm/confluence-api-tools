<?php

namespace VictorStm\confluence\content;

class Element implements IElement
{
    protected $type = '';


    public function build ()
    {
        $res = [
            "type" => $this->type
        ];

        return $res;
    }

}
