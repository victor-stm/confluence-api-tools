<?php

namespace VictorStm\confluence;


use VictorStm\confluence\content\IElement;
use VictorStm\confluence\content\Page;


class Confluence
{
    private $token;
    private $apiUrl;
    private $curlHeaders;


    /**
     * @param string $apiUrl 'domain.without.schema'
     * @param string $token user:api_token
     * @return bool
     */
    public function __construct (string $apiUrl, string $token)
    {
        $this->apiUrl   = $apiUrl;
        $this->token    = $token;

        $this->curlHeaders    = [
            'Authorization: Basic ' . base64_encode ($this->token),
            'content-type: application/json',
            'accept: application/json'
            ];
    }


    /**
     * @param string $id
     * @param IElement $content
     * @param string $title
     * @param string $space
     * @return bool
     */
    public function updatePage (string $id, IElement $content, string $title, string $space)
    {
        try {
            $info       = $this->getPageInfoOrFail ($id);
            $version    = $info->version + 1;
            $title      = $title ? $title : $info->title;

            $this->savePageOrFail ($id, $content, $title, $space, $version);
        } catch (\Exception $e) {
            print_r ($e . "\n");
            return false;
        }
        return true;
    }


     /**
     * @param string $id
     * @return PageInfo
     */
    public function getPageInfoOrFail (string $id)
    {
        $url        = "https://$this->apiUrl/wiki/rest/api/content/$id/?expand=version";
        $answer     = $this->curl ($url, '', 'GET', $this->curlHeaders);

        $decoded        = json_decode ($answer['body']);
        $res            = new PageInfo ();
        $res->title     = $decoded->title;
        $res->version   = $decoded->version->number;

        return $res;
    }


	/**
	 * @param $id
	 * @return PageInfo
	 */
	public function getPageOrFail ($id)
	{
		$url			= "https://$this->apiUrl/wiki/rest/api/content/$id/?expand=body.storage";
		$answer			= $this->curl ($url, '', 'GET', $this->curlHeaders);

		$decoded        = json_decode ($answer['body']);
		$res            = new PageInfo ();
		$res->title     = $decoded->title;
		$res->body      = $decoded->body->storage->value;

		return $res;
	}


    /**
     * @param string $id
     * @param IElement $content
     * @param string $title
     * @param string $space
     * @param int $version
     * @throws \Exception
     */
    public function savePageOrFail (string $id, IElement $content, string $title, string $space, int $version = 0)
    {
        $page           = new Page ($content);
        $page->id       = $id;
        $page->title    = $title;
        $page->space    = $space;
        $page->version  = $version;


        //---------------------------------------------------------------------
        // Send data to confluence
        //---------------------------------------------------------------------

        $url        = "https://$this->apiUrl/wiki/rest/api/content/$id/?status=draft&action=publish";
        $post       = json_encode ($page->build (), JSON_UNESCAPED_UNICODE);
        $answer     = $this->curl ($url, $post, 'PUT', $this->curlHeaders);

        $body       = $answer['body'];
        $decoded    = json_decode ($body);
        $res_id     = $decoded->id ?? 0;

        if ($res_id != $id) throw new \Exception('failed update confluence: ' . $body);
    }


    /**
     * @param string $url
     * @param string $data
     * @param string $method
     * @param array $headers
     * @param int $time_out
     * @return array
     */
    public function curl (string $url, string $data = '', string $method = 'GET', array $headers = [], int $time_out = 25)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_TIMEOUT, $time_out);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_VERBOSE, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        if ($headers) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        if ($data) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }

        $body       = curl_exec($curl);
        $http_code  = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        return [
            'httpCode'  => $http_code,
            'body'      => $body
        ];
    }

}
