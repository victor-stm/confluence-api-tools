<?php


namespace VictorStm\confluence;


class PageInfo
{
    public $title = '';
    public $version = 0;
    public $body = '';
}
